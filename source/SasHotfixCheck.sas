﻿/******************************************************************************/
/* SAS Utility Program                                                        */
/*                                                                            */
/* NAME       : SasHotfixCheck.sas                                            */
/* AUTHOR     : T.Kawakami                                                    */
/* PRODUCT    : ALL                                                           */
/* VERSION    : 9.2, 9.3, 9.4                                                 */
/* PLATFORM   : WINDOWS (WIN, WX6)                                            */
/* CREATED    : 03SEP2019                                                     */
/* MUST       : XCMD system option                                            */
/*            : PowerShell                                                    */
/* NOTES      : This program refers to Usage Note 35968:                      */
/*              Using the ViewRegistry Report and other methods to determine  */
/*              the SAS(r) 9.2 and SAS(r) 9.3 software releases and hot fixes */
/*              that are installed. http://support.sas.com/kb/35/968.html     */
/*            : After submit, you must Yes or Continue in UAC dialog.         */
/*            : Becase a part of hotfixes are released in combination with    */
/*              other hotfix, sometimes cannnot access to created url.        */
/*            : You can use freely under WTFPL.                               */
/*----------------------------------------------------------------------------*/
/* MACRO PARAMETERS                                                           */
/*  sashome   : Specify SASHome directory.                                    */
/*              For example, C:\Program Files\SASHome2                        */
/*  hotfixes  : Descript the HOTFIX ID with space separator                   */
/*              For example, 3Z011 A3Z010                                     */
/*  outputxls : Specify 1 when output Dataset to Excel Sheet.                 */
/*----------------------------------------------------------------------------*/
/* RETURN VALUES                                                              */
/*  SASLOG    : Print whether hot fixes are installed one by one in OK or NG. */
/*  hotfixerr : When at least one hotfix is not installed, shows 1 else 0.    */
/*----------------------------------------------------------------------------*/
/* DATA SETS                                                                  */
/*  WORK.SAS_HOTFIX_REPORT  : SAS product and Installed hotfix list with url  */
/*----------------------------------------------------------------------------*/
/* HISTORY    : CREATED on 03Jun1999                                          */
/******************************************************************************/

%let sashome=C:\Program Files\SASHome2;
%let hotfixes=A3Z011 A3Z010;
%let outputxls=1;


%Macro MCheckHotfix;

  options noxwait xsync nonotes nosource;

  %let jarfile=sas.tools.viewregistry.jar;
  %let outfile=DeploymentRegistry.txt;
  %let batfile=%sysfunc(pathname(work))\sasviewregistry.bat;
  %let sasftp=http://ftp.sas.com/techsup/download/hotfix/HF2/;
  %local hhobs;
  %local param;
  %global hotfixerr;
  %let hotfixerr=0;
  
  filename sasvr "&batfile.";
  data _null_;
    file sasvr;
    put "cd ""&sashome.\deploymntreg""";
    put "java -jar &jarfile.";
  run;
  filename sasvr;
  
  x "powershell start-process """"""&batfile."""""" -verb runas";
  
  data _null_;
    call sleep(2000);
  run;
  
  filename sasrep "&sashome.\deploymntreg\&outfile.";
  data report1;
    length hed $256 rep $256;
    infile sasrep dlm=':' missover;
    input hed rep;
  run;
  filename sasrep;
  
  data report2;
    length host $3
           prod $12
           ver $8
           dispname $80
           dispver $10
           hotfixcnt 8
           hotfix $6
           ;
    retain host prod ver dispname dispver hotfix hotfixcnt;
    set report1;
    if _n_=1 then hotfixcnt=0;
  
    if upcase(hed)='HOST' then host=rep;
    else if upcase(hed)='PRODUCT' then prod=rep;
    else if upcase(hed)='VERSION' then ver=rep;
    else if upcase(hed)='DISPLAY NAME' then dispname=rep;
    else if upcase(hed)='DISPLAY VERSION' then dispver=rep;
    else if upcase(hed)='HOT FIX ENTRY' then do;
      hotfixcnt+1;
      hotfix=scan(rep,-1);
      url="&sasftp."||substr(hotfix,1,3)||'_'||host||'.html#'||hotfix;
      output;
    end;
    else if substr(hed,1,10)='----------' then do;
      if hotfixcnt=0 then output;
      hotfixcnt=0;
      hotfix='';
    end;
    drop hed rep;
  run;
  
  proc sort data=report2 out=report3;
    by dispname host;
  run;
  
  data sas_hotfix_report;
    retain host
           prod
           ver
           dispname
           dispver
           hotfixcnt
           hotfix
           url;
    set report3;
    by dispname host;
  run;
  
  proc delete data=report1-report3;
  run;

  data _null_;
    call symputx('pcnt',count(strip("&hotfixes"),' ')+1);
  run;

  %put ;
  %put ;

  %do i=1 %to &pcnt;
    %let hhobs=0;
    %let param=%scan(&hotfixes,&i);
    data _null_;
      set sas_hotfix_report(where=(upcase(host)=upcase("&SYSSCP.") and 
                                   upcase(hotfix)=upcase("&param.")));
      call symputx('hhobs',_N_);
    run;

    %if &hhobs=0 %then %do;
      %put NG: &param.;
      %let hotfixerr=1;
    %end; %else
    %do;
      %put OK: &param.;
    %end;
  %end;
  
  %if &outputxls.=1 %then %do;
    ods _all_ close;
    ods excel;
    proc print;
    run;
    ods excel close;
  %end;
  
%Mend;

%MCheckHotfix;

